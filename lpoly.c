#include "lpoly.h"
#include "lpoly_descr.h"
#include <stddef.h>

t_lpoly_val lpoly_calc_val(const struct st_lpoly_interval_descr *descr, t_lpoly_val val, int *ok) {
    t_lpoly_val ret = 0;
    int fnd = 0;
    const struct st_lpoly_interval_descr *first = descr;

    while ((descr->calc_func != NULL) && !fnd) {        
        if ((val >= descr->from) && (val <= descr->to)) {
            fnd = 1;
            ret = descr->calc_func(descr, val);
        }
        descr++;
    }

    if (!fnd) {
        const struct st_lpoly_interval_descr *last = descr - 1;
        if (val < first->from) {
            ret = first->calc_func(first, first->from);
        } else if (val > last->to) {
            ret = last->calc_func(last, last->to);
        }
    }

    if (ok!=NULL)
        *ok = fnd;
    return ret;
}
