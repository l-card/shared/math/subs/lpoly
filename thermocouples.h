#ifndef LPOLY_THERMOCOUPLES_H
#define LPOLY_THERMOCOUPLES_H

#include "lpoly.h"


/*  Файл содержит настройки функции lpoly_calc_val() для перевода измеренного значения
   ЭДС термопары в температуру. На вход функции lpoly_calc_val() в качестве
   параметров должены быть переданы параметры нужного типа термопары из
   объявленных ниже, а в качестве значения - измеренное ЭДС в мс.
   Функция lpoly_calc_val() вернет значение температуры при темеретуре холодного
   спая 0 градусов, т.е. к полученному значению еще нужно прибавить температуру
   холодного спая */

#ifdef __cplusplus
extern "C" {
#endif

extern const t_lpoly_interval_descr *const lpoly_thermocouple_r;
extern const t_lpoly_interval_descr *const lpoly_thermocouple_s;
extern const t_lpoly_interval_descr *const lpoly_thermocouple_b;
extern const t_lpoly_interval_descr *const lpoly_thermocouple_j;
extern const t_lpoly_interval_descr *const lpoly_thermocouple_t;
extern const t_lpoly_interval_descr *const lpoly_thermocouple_e;
extern const t_lpoly_interval_descr *const lpoly_thermocouple_k;
extern const t_lpoly_interval_descr *const lpoly_thermocouple_n;
extern const t_lpoly_interval_descr *const lpoly_thermocouple_a1;
extern const t_lpoly_interval_descr *const lpoly_thermocouple_a2;
extern const t_lpoly_interval_descr *const lpoly_thermocouple_a3;
extern const t_lpoly_interval_descr *const lpoly_thermocouple_l;
extern const t_lpoly_interval_descr *const lpoly_thermocouple_m;
#ifdef __cplusplus
}
#endif

#endif // LPOLY_THERMOCOUPLES_H
