#ifndef LPOLY_RTD_H
#define LPOLY_RTD_H

#include "lpoly.h"

/* Файл содержит настройки функции lpoly_calc_val() для перевода измеренного значения
   сопротивления в температуру. На вход функции lpoly_calc_val() в качестве
   параметров должены быть переданы параметры нужного термосопротивления из
   объявленных ниже, а в качестве значения - отношение R/R0, где R -
   измеренное сопротивление, а R0 - сопротивление при 0 градусов */

#ifdef __cplusplus
extern "C" {
#endif
/* Платиновое термосопротивление с коэф. 0.00385 (от -200 до +850) */
extern const t_lpoly_interval_descr *const lpoly_rtd_platinum_385;
/* Платиновое термосопротивление с коэф. 0.00391 (от -200 до +850) */
extern const t_lpoly_interval_descr *const lpoly_rtd_platinum_391;
/* Никелевое термосопротивление с коэф. 0.00617 (от -60 до +180) */
extern const t_lpoly_interval_descr *const lpoly_rtd_nickel;
/* Медное термосопротивление с коэф. 0.00428 (от -180  до +200)*/
extern const t_lpoly_interval_descr *const lpoly_rtd_copper;
#ifdef __cplusplus
}
#endif
#endif // LPOLY_RTD_H
