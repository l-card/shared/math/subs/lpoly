#include "thermocouples.h"
#include <stdlib.h>
#include "lpoly_descr.h"

static t_lpoly_val f_tcouple_func(const struct st_lpoly_interval_descr *poly, t_lpoly_val val);

/* ------------------------------------ Тип R --------------------------------*/
static const t_lpoly_val cr_1[] = {
    0,
    (t_lpoly_val)1.8891380e2,
    (t_lpoly_val)-9.3835290e1,
    (t_lpoly_val)1.3068619e2,
    (t_lpoly_val)-2.2703580e2,
    (t_lpoly_val)3.5145659e2,
    (t_lpoly_val)-3.8953900e2,
    (t_lpoly_val)2.8239471e2,
    (t_lpoly_val)-1.2607281e2,
    (t_lpoly_val)3.1353611e1,
    (t_lpoly_val)-3.3187769
};

static const t_lpoly_val cr_2[] = {
    (t_lpoly_val)1.334584505e1,
    (t_lpoly_val)1.472644573e2,
    (t_lpoly_val)1.844024844e1,
    (t_lpoly_val)4.031129726,
    (t_lpoly_val)-6.249428360e-1,
    (t_lpoly_val)6.468412046e-2,
    (t_lpoly_val)-4.458750426e-3,
    (t_lpoly_val)1.994710149e-4,
    (t_lpoly_val)-5.313401790e-6,
    (t_lpoly_val)6.481976217e-8
};

static const t_lpoly_val cr_3[] = {
    (t_lpoly_val)-8.199599416e1,
    (t_lpoly_val)1.553962042e2,
    (t_lpoly_val)-8.342197663,
    (t_lpoly_val)4.279433549e-1,
    (t_lpoly_val)-1.191577910e-2,
    (t_lpoly_val)1.492290091e-4
};

static const t_lpoly_val cr_4[] = {
    (t_lpoly_val)3.406177836e4,
    (t_lpoly_val)-7.023729171e3,
    (t_lpoly_val)5.582903813e2,
    (t_lpoly_val)-1.952394635e1,
    (t_lpoly_val)2.560740231e-1
};



static const t_lpoly_interval_descr thermocouple_r_array[] = {
    {(t_lpoly_val)-0.226, (t_lpoly_val)1.923,    f_tcouple_func, cr_1, sizeof(cr_1)/sizeof(cr_1[0]), NULL},
    {(t_lpoly_val)1.923,  (t_lpoly_val)11.361,   f_tcouple_func, cr_2, sizeof(cr_2)/sizeof(cr_2[0]), NULL},
    {(t_lpoly_val)11.361, (t_lpoly_val)19.739,   f_tcouple_func, cr_3, sizeof(cr_3)/sizeof(cr_3[0]), NULL},
    {(t_lpoly_val)19.739, (t_lpoly_val)21.103,   f_tcouple_func, cr_4, sizeof(cr_4)/sizeof(cr_4[0]), NULL},
    LPOLY_DESCR_TABLE_END
};
const t_lpoly_interval_descr *const lpoly_thermocouple_r = &thermocouple_r_array[0];


/* ------------------------------------ Тип S --------------------------------*/
static const t_lpoly_val cs_1[] = {
    0,
    (t_lpoly_val)1.84949460e2,
    (t_lpoly_val)-8.00504062e1,
    (t_lpoly_val)1.02237430e2,
    (t_lpoly_val)-1.52248592e2,
    (t_lpoly_val)1.88821343e2,
    (t_lpoly_val)-1.59085941e2,
    (t_lpoly_val)8.23027880e1,
    (t_lpoly_val)-2.34181944e1,
    (t_lpoly_val)2.79786260
};

static const t_lpoly_val cs_2[] = {
    (t_lpoly_val)1.291507177e1,
    (t_lpoly_val)1.466298863e2,
    (t_lpoly_val)-1.534713402e1,
    (t_lpoly_val)3.145945973,
    (t_lpoly_val)-4.163257839e-1,
    (t_lpoly_val)3.187963771e-2,
    (t_lpoly_val)-1.291637500e-3,
    (t_lpoly_val)2.183475087e-5,
    (t_lpoly_val)-1,447379511e-7,
    (t_lpoly_val)8.211272125e-9
};

static const t_lpoly_val cs_3[] = {
    (t_lpoly_val)-8.087801117e1,
    (t_lpoly_val)1.621573104e2,
    (t_lpoly_val)-8.536869453,
    (t_lpoly_val)4.719686976e-1,
    (t_lpoly_val)-1.441693666e-2,
    (t_lpoly_val)2.081618890e-4
};

static const t_lpoly_val cs_4[] = {
    (t_lpoly_val)5.333875126e4,
    (t_lpoly_val)-1.235892298e4,
    (t_lpoly_val)1.092657613e3,
    (t_lpoly_val)-4.265693686e1,
    (t_lpoly_val)6.247205420e-1
};

static const t_lpoly_interval_descr thermocouple_s_array[] = {
    {(t_lpoly_val)-0.235, (t_lpoly_val)1.874,    f_tcouple_func, cs_1, sizeof(cs_1)/sizeof(cs_1[0]), NULL},
    {(t_lpoly_val)1.874,  (t_lpoly_val)10.332,   f_tcouple_func, cs_2, sizeof(cs_2)/sizeof(cs_2[0]), NULL},
    {(t_lpoly_val)10.332, (t_lpoly_val)17.536,   f_tcouple_func, cs_3, sizeof(cs_3)/sizeof(cs_3[0]), NULL},
    {(t_lpoly_val)17.536, (t_lpoly_val)18.694,   f_tcouple_func, cs_4, sizeof(cs_4)/sizeof(cs_4[0]), NULL},
    LPOLY_DESCR_TABLE_END
};
const t_lpoly_interval_descr *const lpoly_thermocouple_s = &thermocouple_s_array[0];

/* ------------------------------------ Тип B --------------------------------*/
static const t_lpoly_val cb_1[] = {
    (t_lpoly_val)9.8423321e1,
    (t_lpoly_val)6.9971500e2,
    (t_lpoly_val)-8.4765304e2,
    (t_lpoly_val)1.0052644e3,
    (t_lpoly_val)-8.3345952e2,
    (t_lpoly_val)4.5508542e2,
    (t_lpoly_val)-1.5523037e2,
    (t_lpoly_val)2.9886750e1,
    (t_lpoly_val)-2.4742860,
};

static const t_lpoly_val cb_2[] = {
    (t_lpoly_val)2.1315071e2,
    (t_lpoly_val)2.8510504e2,
    (t_lpoly_val)-5.2742887e1,
    (t_lpoly_val)9.9160804,
    (t_lpoly_val)-1.2965303,
    (t_lpoly_val)1.1195870e-1,
    (t_lpoly_val)-6.0625199e-3,
    (t_lpoly_val)1.8661696e-4,
    (t_lpoly_val)-2.4878585e-6
};

static const t_lpoly_interval_descr thermocouple_b_array[] = {
    {(t_lpoly_val)0.291, (t_lpoly_val)2.431,    f_tcouple_func, cb_1, sizeof(cb_1)/sizeof(cb_1[0]), NULL},
    {(t_lpoly_val)2.431, (t_lpoly_val)13.820,   f_tcouple_func, cb_2, sizeof(cb_2)/sizeof(cb_2[0]), NULL},
    LPOLY_DESCR_TABLE_END
};
const t_lpoly_interval_descr *const lpoly_thermocouple_b = &thermocouple_b_array[0];


/* ------------------------------------ Тип J --------------------------------*/
static const t_lpoly_val cj_1[] = {
    0,
    (t_lpoly_val)1.9528268e1,
    (t_lpoly_val)-1.2286185,
    (t_lpoly_val)-1.0752178,
    (t_lpoly_val)-5.9086933e-1,
    (t_lpoly_val)-1.7256713e-1,
    (t_lpoly_val)-2.8131513e-2,
    (t_lpoly_val)-2.3963370e-3,
    (t_lpoly_val)-8.3823321e-5
};

static const t_lpoly_val cj_2[] = {
    0,
    (t_lpoly_val)1.978425e1,
    (t_lpoly_val)-2.001204e-1,
    (t_lpoly_val)1.036969e-2,
    (t_lpoly_val)-2.549687e-4,
    (t_lpoly_val)3.585153e-6,
    (t_lpoly_val)-5.344285e-8,
    (t_lpoly_val)5.099890e-10
};

static const t_lpoly_val cj_3[] = {
    (t_lpoly_val)-3.11358187e3,
    (t_lpoly_val)3.00543684e2,
    (t_lpoly_val)-9.94773230,
    (t_lpoly_val)1.70276630e-1,
    (t_lpoly_val)-1.43033468e-3,
    (t_lpoly_val)4.73886084e-6
};

static const t_lpoly_interval_descr thermocouple_j_array[] = {
    {(t_lpoly_val)-8.095, 0,                    f_tcouple_func, cj_1, sizeof(cj_1)/sizeof(cj_1[0]), NULL},
    {0,      (t_lpoly_val)42.919,               f_tcouple_func, cj_2, sizeof(cj_2)/sizeof(cj_2[0]), NULL},
    {(t_lpoly_val)42.919, (t_lpoly_val)69.553,  f_tcouple_func, cj_3, sizeof(cj_3)/sizeof(cj_3[0]), NULL},
    LPOLY_DESCR_TABLE_END
};
const t_lpoly_interval_descr *const lpoly_thermocouple_j = &thermocouple_j_array[0];

/* ------------------------------------ Тип T --------------------------------*/
static const t_lpoly_val ct_1[] = {
    0,
    (t_lpoly_val)2.5949192e1,
    (t_lpoly_val)-2.1316967e-1,
    (t_lpoly_val)7.9018692e-1,
    (t_lpoly_val)4.2527777e-1,
    (t_lpoly_val)1.3304473e-1,
    (t_lpoly_val)2.0241446e-2,
    (t_lpoly_val)1.2668171e-3
};

static const t_lpoly_val ct_2[] = {
    0,
    (t_lpoly_val)2.592800e1,
    (t_lpoly_val)-7.602961e-1,
    (t_lpoly_val)4.637791e-2,
    (t_lpoly_val)-2.165394e-3,
    (t_lpoly_val)6.048144e-5,
    (t_lpoly_val)-7.293422e-7
};

static const t_lpoly_interval_descr thermocouple_t_array[] = {
    {(t_lpoly_val)-5.603,      0, f_tcouple_func, ct_1, sizeof(ct_1)/sizeof(ct_1[0]), NULL},
    {0,      (t_lpoly_val)20.872, f_tcouple_func, ct_2, sizeof(ct_2)/sizeof(ct_2[0]), NULL},
    LPOLY_DESCR_TABLE_END
};
const t_lpoly_interval_descr *const lpoly_thermocouple_t = &thermocouple_t_array[0];


/* ------------------------------------ Тип E --------------------------------*/
static const t_lpoly_val ce_1[] = {
    0,
    (t_lpoly_val)1.6977288e1,
    (t_lpoly_val)-4.3514970e-1,
    (t_lpoly_val)-1.5859697e-1,
    (t_lpoly_val)-9.2502871e-2,
    (t_lpoly_val)-2.6084314e-2,
    (t_lpoly_val)-4.1360199e-3,
    (t_lpoly_val)-3.4034030e-4,
    (t_lpoly_val)-1.1564890e-5
};

static const t_lpoly_val ce_2[] = {
    0,
    (t_lpoly_val)1.7057035e1,
    (t_lpoly_val)-2.3301759e-1,
    (t_lpoly_val)6.5435585e-3,
    (t_lpoly_val)-7.3562749e-5,
    (t_lpoly_val)-1.7896001e-6,
    (t_lpoly_val)8.4036165e-8,
    (t_lpoly_val)-1.3735879e-9,
    (t_lpoly_val)1.0629823e-11,
    (t_lpoly_val)-3.2447087e-14
};

static const t_lpoly_interval_descr thermocouple_e_array[] = {
    {(t_lpoly_val)-8.825,      0, f_tcouple_func, ce_1, sizeof(ce_1)/sizeof(ce_1[0]), NULL},
    {0,      (t_lpoly_val)76.373, f_tcouple_func, ce_2, sizeof(ce_2)/sizeof(ce_2[0]), NULL},
    LPOLY_DESCR_TABLE_END
};
const t_lpoly_interval_descr *const lpoly_thermocouple_e = &thermocouple_e_array[0];

/* ------------------------------------ Тип K --------------------------------*/
static const t_lpoly_val ck_1[] = {
    0,
    (t_lpoly_val)2.5173462e1,
    (t_lpoly_val)-1.1662878,
    (t_lpoly_val)-1.0833638,
    (t_lpoly_val)-8.9773540e-1,
    (t_lpoly_val)-3.7342377e-1,
    (t_lpoly_val)-8.6632643e-2,
    (t_lpoly_val)-1.0450598e-2,
    (t_lpoly_val)-5.1920577e-4
};

static const t_lpoly_val ck_2[] = {
    0,
    (t_lpoly_val)2.508355e1,
    (t_lpoly_val)7.860106e-2,
    (t_lpoly_val)-2.503131e-1,
    (t_lpoly_val)8.315270e-2,
    (t_lpoly_val)-1.228034e-2,
    (t_lpoly_val)9.804036e-4,
    (t_lpoly_val)-4.413030e-5,
    (t_lpoly_val)1.057734e-6,
    (t_lpoly_val)-1.052755e-8
};

static const t_lpoly_val ck_3[] = {
    (t_lpoly_val)-1.318058e2,
    (t_lpoly_val)4.830222e1,
    (t_lpoly_val)-1.646031,
    (t_lpoly_val)5.464731e-2,
    (t_lpoly_val)-9.650715e-4,
    (t_lpoly_val)8.802193e-6,
    (t_lpoly_val)-3.110810e-8
};

static const t_lpoly_interval_descr thermocouple_k_array[] = {
    {(t_lpoly_val)-5.891,                   0, f_tcouple_func, ck_1, sizeof(ck_1)/sizeof(ck_1[0]), NULL},
    {0,                   (t_lpoly_val)20.644, f_tcouple_func, ck_2, sizeof(ck_2)/sizeof(ck_2[0]), NULL},
    {(t_lpoly_val)20.644, (t_lpoly_val)54.886, f_tcouple_func, ck_3, sizeof(ck_3)/sizeof(ck_3[0]), NULL},
    LPOLY_DESCR_TABLE_END
};
const t_lpoly_interval_descr *const lpoly_thermocouple_k = &thermocouple_k_array[0];

/* ------------------------------------ Тип N --------------------------------*/
static const t_lpoly_val cn_1[] = {
    0,
    (t_lpoly_val)3.8436847e1,
    (t_lpoly_val)1.1010485,
    (t_lpoly_val)5.2229312,
    (t_lpoly_val)7.2060525,
    (t_lpoly_val)5.8488586,
    (t_lpoly_val)2.7754916,
    (t_lpoly_val)7.7075166e-1,
    (t_lpoly_val)1.1582665e-1,
    (t_lpoly_val)7.3138868e-3
};

static const t_lpoly_val cn_2[] = {
    0,
    (t_lpoly_val)3.86896e1,
    (t_lpoly_val)-1.08267,
    (t_lpoly_val)4.70205e-2,
    (t_lpoly_val)-2.12169e-6,
    (t_lpoly_val)-1.17272e-4,
    (t_lpoly_val)5.39280e-6,
    (t_lpoly_val)-7.98156e-8
};

static const t_lpoly_val cn_3[] = {
    (t_lpoly_val)1.972485e1,
    (t_lpoly_val)3.300943e1,
    (t_lpoly_val)-3.915159e-1,
    (t_lpoly_val)9.855391e-3,
    (t_lpoly_val)-1.274371e-4,
    (t_lpoly_val)7.767022e-7
};


static const t_lpoly_interval_descr thermocouple_n_array[] = {
    {(t_lpoly_val)-3.990,                   0, f_tcouple_func, cn_1, sizeof(cn_1)/sizeof(cn_1[0]), NULL},
    {0,                   (t_lpoly_val)20.613, f_tcouple_func, cn_2, sizeof(cn_2)/sizeof(cn_2[0]), NULL},
    {(t_lpoly_val)20.613, (t_lpoly_val)47.513, f_tcouple_func, cn_3, sizeof(cn_3)/sizeof(cn_3[0]), NULL},
    LPOLY_DESCR_TABLE_END
};
const t_lpoly_interval_descr *const lpoly_thermocouple_n = &thermocouple_n_array[0];




/* ------------------------------------ Тип A-1 --------------------------------*/
static const t_lpoly_val ca1_1[] = {
    (t_lpoly_val)0.9643027,
    (t_lpoly_val)7.9495086e1,
    (t_lpoly_val)-4.9990310,
    (t_lpoly_val)0.6341776,
    (t_lpoly_val)-4.7440967e-2,
    (t_lpoly_val)2.1811337e-3,
    (t_lpoly_val)-5.8324228e-5,
    (t_lpoly_val)8.2433725e-7,
    (t_lpoly_val)-4.5928480e-9
};

static const t_lpoly_interval_descr thermocouple_a1_array[] = {
    {0,  (t_lpoly_val)33.640, f_tcouple_func, ca1_1, sizeof(ca1_1)/sizeof(ca1_1[0]), NULL},
    LPOLY_DESCR_TABLE_END
};
const t_lpoly_interval_descr *const lpoly_thermocouple_a1 = &thermocouple_a1_array[0];

/* ------------------------------------ Тип A-2 --------------------------------*/
static const t_lpoly_val ca2_1[] = {
    (t_lpoly_val)1.1196428,
    (t_lpoly_val)8.0569397e1,
    (t_lpoly_val)-6.2279122,
    (t_lpoly_val)0.9337015,
    (t_lpoly_val)-8.2608051e-2,
    (t_lpoly_val)4.4110979e-3,
    (t_lpoly_val)-1.3610551e-4,
    (t_lpoly_val)2.2183851e-6,
    (t_lpoly_val)-1.4527698e-8
};

static const t_lpoly_interval_descr thermocouple_a2_array[] = {
    {0,  (t_lpoly_val)27.232, f_tcouple_func, ca2_1, sizeof(ca2_1)/sizeof(ca2_1[0]), NULL},
    LPOLY_DESCR_TABLE_END
};
const t_lpoly_interval_descr *const lpoly_thermocouple_a2 = &thermocouple_a2_array[0];

/* ------------------------------------ Тип A-3 --------------------------------*/
static const t_lpoly_val ca3_1[] = {
    (t_lpoly_val)0.8769216,
    (t_lpoly_val)8,1483231e1,
    (t_lpoly_val)-5.9344173,
    (t_lpoly_val)0.8699340,
    (t_lpoly_val)-7.6797687e-2,
    (t_lpoly_val)4.1814387e-3,
    (t_lpoly_val)-1.3439670e-4,
    (t_lpoly_val)2.342409e-6,
    (t_lpoly_val)-1.6988727e-8
};

static const t_lpoly_interval_descr thermocouple_a3_array[] = {
    {0,  (t_lpoly_val)26.773 , f_tcouple_func, ca3_1, sizeof(ca3_1)/sizeof(ca3_1[0]), NULL},
    LPOLY_DESCR_TABLE_END
};
const t_lpoly_interval_descr *const lpoly_thermocouple_a3 = &thermocouple_a3_array[0];


/* ------------------------------------ Тип L --------------------------------*/
static const t_lpoly_val cl_1[] = {
    (t_lpoly_val)1.1573067e-4,
    (t_lpoly_val)1.5884573e1,
    (t_lpoly_val)4.0458554e-2,
    (t_lpoly_val)0.3170064,
    (t_lpoly_val)0.1666128,
    (t_lpoly_val)5.1946958e-2,
    (t_lpoly_val)9.5288883e-3,
    (t_lpoly_val)1.0301283e-3,
    (t_lpoly_val)6.0654431e-5,
    (t_lpoly_val)1.5131878e-6
};

static const t_lpoly_val cl_2[] = {
    (t_lpoly_val)7.2069422e-3,
    (t_lpoly_val)1.5775525e1,
    (t_lpoly_val)-0.2261183,
    (t_lpoly_val)9.4286756e-3,
    (t_lpoly_val)-3.5394655e-4,
    (t_lpoly_val)1.0050886e-5,
    (t_lpoly_val)-1.9323678e-7,
    (t_lpoly_val)2.3816891e-9,
    (t_lpoly_val)-1.7130654e-11,
    (t_lpoly_val)5.4857331e-14
};


static const t_lpoly_interval_descr thermocouple_l_array[] = {
    {(t_lpoly_val)-9.488, 0,  f_tcouple_func, cl_1, sizeof(cl_1)/sizeof(cl_1[0]), NULL},
    {0, (t_lpoly_val)66.466,  f_tcouple_func, cl_2, sizeof(cl_2)/sizeof(cl_2[0]), NULL},
    LPOLY_DESCR_TABLE_END
};
const t_lpoly_interval_descr *const lpoly_thermocouple_l = &thermocouple_l_array[0];


/* ------------------------------------ Тип M --------------------------------*/
static const t_lpoly_val cm_1[] = {
    (t_lpoly_val)0.4548090,
    (t_lpoly_val)2.2657698e-2,
    (t_lpoly_val)-7.7935652e-7,
    (t_lpoly_val)1.1786931e-10
};

static const t_lpoly_interval_descr thermocouple_m_array[] = {
    {(t_lpoly_val)-6.154,  (t_lpoly_val)4.722, f_tcouple_func, cm_1, sizeof(cm_1)/sizeof(cm_1[0]), NULL},
    LPOLY_DESCR_TABLE_END
};
const t_lpoly_interval_descr *const lpoly_thermocouple_m = &thermocouple_m_array[0];







/* ---------------------------------------------------------------------------*/

static t_lpoly_val f_tcouple_func(const struct st_lpoly_interval_descr *poly, t_lpoly_val val) {
    t_lpoly_val e_mul = 1;
    t_lpoly_val ret = 0;
    for (unsigned i = 0; i < poly->coef_cnt; i++) {
        if (i!=0)
            e_mul*=val;
        ret += e_mul * poly->coef[i];
    }
    return ret;
}



