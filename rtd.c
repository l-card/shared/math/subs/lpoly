#include "rtd.h"
#include "lpoly_descr.h"
#include <math.h>
#include <stdlib.h>

static t_lpoly_val f_rtd_ply_func(const struct st_lpoly_interval_descr *poly, t_lpoly_val val);
static t_lpoly_val f_rtd_ply_nickel_func(const struct st_lpoly_interval_descr *poly, t_lpoly_val val);
static t_lpoly_val f_rtd_sqrt_func(const struct st_lpoly_interval_descr *poly, t_lpoly_val val);
static t_lpoly_val f_rtd_div_func(const struct st_lpoly_interval_descr *poly, t_lpoly_val val);

static const t_lpoly_val rtd_pl_385_d[] = {
    (t_lpoly_val)255.819,
    (t_lpoly_val)9.1455,
    (t_lpoly_val)-2.92363,
    (t_lpoly_val)1.7909
};
static const t_lpoly_val rtd_pl_385_ab[] = {
    (t_lpoly_val)3.9083e-3,
    (t_lpoly_val)-5.775e-7
};
static const t_lpoly_interval_descr rtd_platinum_385_array[] = {
    {(t_lpoly_val)0.1,         1, f_rtd_ply_func,  rtd_pl_385_d,  sizeof(rtd_pl_385_d)/sizeof(rtd_pl_385_d[0]), NULL},
    {1,      (t_lpoly_val)3.9048, f_rtd_sqrt_func, rtd_pl_385_ab, sizeof(rtd_pl_385_ab)/sizeof(rtd_pl_385_ab[0]), NULL},
    LPOLY_DESCR_TABLE_END
};

const t_lpoly_interval_descr *const lpoly_rtd_platinum_385 = &rtd_platinum_385_array[0];


static const t_lpoly_val rtd_pl_391_d[] = {
    (t_lpoly_val)251.903,
    (t_lpoly_val)8.80035,
    (t_lpoly_val)-2.91506,
    (t_lpoly_val)1.67611
};
static const t_lpoly_val rtd_pl_391_ab[] = {
    (t_lpoly_val)3.9690e-3,
    (t_lpoly_val)-5.841e-7
};
static const t_lpoly_interval_descr rtd_platinum_391_array[] = {
    {(t_lpoly_val)0.1,         1, f_rtd_ply_func,  rtd_pl_391_d,  sizeof(rtd_pl_391_d)/sizeof(rtd_pl_391_d[0]), NULL},
    {1,      (t_lpoly_val)3.9516, f_rtd_sqrt_func, rtd_pl_391_ab, sizeof(rtd_pl_391_ab)/sizeof(rtd_pl_391_ab[0]), NULL},
    LPOLY_DESCR_TABLE_END
};
const t_lpoly_interval_descr *const lpoly_rtd_platinum_391 = &rtd_platinum_391_array[0];


static const t_lpoly_val rtd_nickel_d[] = {
    (t_lpoly_val)144.096,
    (t_lpoly_val)-25.502,
    (t_lpoly_val)4.4876
};
static const t_lpoly_val rtd_nickel_ab[] = {
    (t_lpoly_val)5.4963e-3,
    (t_lpoly_val)6.7556e-6
};
static const t_lpoly_interval_descr rtd_nickel_array[] = {
    {(t_lpoly_val)0.1,    (t_lpoly_val)1.6172, f_rtd_sqrt_func,       rtd_nickel_ab,  sizeof(rtd_nickel_ab)/sizeof(rtd_nickel_ab[0]), NULL},
    {(t_lpoly_val)1.6172, (t_lpoly_val)2.5,    f_rtd_ply_nickel_func, rtd_nickel_d, sizeof(rtd_nickel_d)/sizeof(rtd_nickel_d[0]), NULL},
    LPOLY_DESCR_TABLE_END
};
const t_lpoly_interval_descr *const lpoly_rtd_nickel = &rtd_nickel_array[0];

static const t_lpoly_val rtd_copper_d[] = {
    (t_lpoly_val)233.87,
    (t_lpoly_val)7.9370,
    (t_lpoly_val)-2.0062,
    (t_lpoly_val)-0.3953
};
static const t_lpoly_val rtd_copper_a[] = {
    (t_lpoly_val)4.28e-3
};
static const t_lpoly_interval_descr rtd_copper_array[] = {
    {(t_lpoly_val)0.1,          1, f_rtd_ply_func,        rtd_copper_d,  sizeof(rtd_copper_d)/sizeof(rtd_copper_d[0]), NULL},
    {1.,         (t_lpoly_val)2.0, f_rtd_div_func,        rtd_copper_a, sizeof(rtd_copper_a)/sizeof(rtd_copper_a[0]), NULL},
    LPOLY_DESCR_TABLE_END
};
const t_lpoly_interval_descr *const lpoly_rtd_copper = &rtd_copper_array[0];



static t_lpoly_val f_rtd_ply_func(const struct st_lpoly_interval_descr *poly, t_lpoly_val val) {
    t_lpoly_val rd_cur = 1;
    t_lpoly_val res = 0;

    for (unsigned i = 0; i < poly->coef_cnt; i++) {
        rd_cur *= (val - 1);
        res += rd_cur*poly->coef[i];
    }
    return res;
}

static t_lpoly_val f_rtd_ply_nickel_func(const struct st_lpoly_interval_descr *poly, t_lpoly_val val) {
    t_lpoly_val rd_cur = 1;
    t_lpoly_val res = 100;

    for (unsigned i = 0; i < poly->coef_cnt; i++) {
        rd_cur *= (val - (t_lpoly_val)1.6172);
        res += rd_cur*poly->coef[i];
    }
    return res;
}

static t_lpoly_val f_rtd_sqrt_func(const struct st_lpoly_interval_descr *poly, t_lpoly_val val) {
    t_lpoly_val a = poly->coef[0];
    t_lpoly_val b = poly->coef[1];
    return ((t_lpoly_val)sqrt(a*a - b*4*((t_lpoly_val)1. - val)) - a)/(b*2);
}

static t_lpoly_val f_rtd_div_func(const struct st_lpoly_interval_descr *poly, t_lpoly_val val) {
    t_lpoly_val a = poly->coef[0];
    return (val - 1)/a;
}


