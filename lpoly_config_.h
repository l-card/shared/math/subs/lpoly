#ifndef LPOLY_CONFIG_H
#define LPOLY_CONFIG_H

/* Пример файла конфигурации для функции lpoly.h. Файл должен быть скопирован в
   проект и переименован в lpoly_config.h.
   при необходимости нужно изменить настройки для своего проекта */


/* тип элемента над которым выполняются преобразования.
   может быть определен как float для встаиваемых систем для ускорения вычислений
   или может быть определен как double для большей точности */
typedef double t_lpoly_val;

#endif // LPOLY_CONFIG_H
